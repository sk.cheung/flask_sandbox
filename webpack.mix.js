let mix = require('laravel-mix');

mix.options({
    processCssUrls: false
});

mix.js('assets/js/main.js', 'flask_sandbox/static/js')
mix.sass('assets/sass/main.scss', 'flask_sandbox/static/css')