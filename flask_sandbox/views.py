from flask import Flask, render_template, redirect, request, url_for
from flask_admin import Admin, AdminIndexView, expose
from flask_admin.contrib.sqla import ModelView
from flask_classful import FlaskView, route
from flask_sandbox.app import db
from flask_sandbox.forms import CreateArticleForm
from flask_sandbox.models import Article, User, Role
from flask_security import login_required, current_user


admin = Admin(template_mode='bootstrap3',
              base_template='admin/admin_base.html')


class ArticlesView(FlaskView):
    def index(self):
        articles = Article.query.all()
        return render_template('articles/list.html', articles=articles)

    @route('/<int:id>/')
    def get(self, id):
        article = Article.query.get_or_404(id)
        return render_template('articles/detail.html', article=article)

    @login_required
    @route('/create/', methods=('GET', 'POST'))
    def create(self):
        form = CreateArticleForm()

        if request.method == 'POST' and form.validate_on_submit():
            article = Article(title=form.title.data, content=form.content.data)
            db.session.add(article)
            db.session.commit()
            return redirect('/articles')

        return render_template('articles/create.html', form=form)


class IndexAdminView(AdminIndexView):
    @expose('/')
    def index(self):
        if not current_user.is_authenticated:
            return redirect(url_for('security.login'))
        return super(IndexAdminView, self).index()

    def is_accessible(self):
        return current_user.is_authenticated

    def inaccessible_callback(self, name, **kwargs):
        if not self.is_accessible():
            return redirect(url_for('security.login', next=request.url))


class ArticleAdminView(ModelView):
    can_export = True
    export_types = ('csv', 'json', 'xlsx')
    column_list = ('title', 'content', 'created_at', 'updated_at')
    column_searchable_list = ('title', 'content')
    column_filters = ('created_at', 'updated_at')
    column_editable_list = ('title', 'content')
    form_excluded_columns = ('created_at', 'updated_at')


class UserAdminView(ModelView):
    can_export = True
    column_exclude_list = ('password',)
    form_excluded_columns = ('password',)


class RoleAdminView(ModelView):
    can_export = False


admin.add_view(ArticleAdminView(Article, db.session))
admin.add_view(UserAdminView(User, db.session, category='Users & Groups'))
admin.add_view(RoleAdminView(Role, db.session, category='Users & Groups'))
