from datetime import datetime
from flask_security import UserMixin, RoleMixin
from flask_sqlalchemy import SQLAlchemy, declarative_base
from sqlalchemy import Column, DateTime, Integer, ForeignKey, Text, String, Boolean, Table
from sqlalchemy.orm import relationship, backref

db = SQLAlchemy()

Base = declarative_base()


class TimestampMixin(object):
    created_at = Column(DateTime, nullable=False,
                        default=datetime.utcnow)
    updated_at = Column(DateTime, onupdate=datetime.utcnow)


class Article(db.Model, TimestampMixin):
    id = Column(Integer, primary_key=True)
    title = Column(String(255))
    content = Column(Text())

    def __repr__(self):
        return '<Article %r>' % (self.title)


roles_users = db.Table('roles_users', Column('user_id', Integer, ForeignKey(
    'user.id')), Column('role_id', Integer, ForeignKey('role.id')))


class Role(db.Model, RoleMixin):
    id = Column(Integer(), primary_key=True)
    name = Column(String(80), unique=True)
    description = Column(String(255))

    def __repr__(self):
        return self.name


class User(db.Model, UserMixin):
    id = Column(Integer(), primary_key=True)
    email = Column(String(255), unique=True)
    password = Column(String(255))
    active = Column(Boolean())
    confirmed_at = Column(DateTime())
    last_login_at = Column(DateTime())
    current_login_at = Column(DateTime())
    last_login_ip = Column(String(255))
    current_login_ip = Column(String(255))
    login_count = Column(Integer())
    roles = relationship('Role', secondary=roles_users,
                         backref=backref('users', lazy='dynamic'))

    def __repr__(self):
        return self.email
