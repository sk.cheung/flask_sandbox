from flask import Flask, render_template, redirect, request, url_for
from flask_admin import Admin
from flask_admin import helpers as admin_helpers
from flask_debugtoolbar import DebugToolbarExtension
from flask_migrate import Migrate
from flask_sandbox.filters import datetimeformat
from flask_sandbox.forms import CreateArticleForm
from flask_sandbox.models import db, Article, User, Role
from flask_sandbox.views import ArticlesView, IndexAdminView, admin
from flask_security import SQLAlchemyUserDatastore, Security, login_required
import os

app = Flask(__name__)
app.config['SECRET_KEY'] = os.environ.get('SECRET_KEY', default=None)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get(
    'SQLALCHEMY_DATABASE_URI', default=None)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config['SECURITY_TRACKABLE'] = True
app.config['SECURITY_CONFIRMABLE'] = True
app.config['SECURITY_REGISTERABLE'] = True
app.config['SECURITY_RECOVERABLE'] = True
app.config['SECURITY_CHANGEABLE'] = True
app.config['SECURITY_PASSWORD_SALT'] = os.environ.get(
    'SECRET_KEY', default=None)
db.init_app(app)
user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore)
toolbar = DebugToolbarExtension(app)
migrate = Migrate(app, db)
admin.init_app(app, index_view=IndexAdminView(),
               endpoint='.admin', url='/admin')


@app.route('/')
def index():
    return render_template('index.html', trailing_slash=False)


ArticlesView.register(app)


@security.context_processor
def security_context_processor():
    return dict(
        admin_base_template=admin.base_template,
        admin_view=admin.index_view,
        h=admin_helpers,
        get_url=url_for
    )


app.jinja_env.filters['datetimeformat'] = datetimeformat
